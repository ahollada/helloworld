﻿using System.Web.Configuration;
using HelloWorld.DAO;
using HelloWorld.DAO.Interface;

namespace HelloWorld.BAO
{
    public class MessageLogic
    {
        public MessageLogic(string messageType)
        {
            this.SetupMessageActivator(messageType);
        }

        public IActivateMessage ActivateMessage { get; private set; }

        private void SetupMessageActivator(string messageType)
        {
            ActivateMessage = ActivateMessageFactory.CreateActiveMessage(messageType);
        }


        public void DeliverMessageAccordingly(string message)
        {
          //do any logic here
            ActivateMessage.ActivateMessage(message);
            
        }
    }
}