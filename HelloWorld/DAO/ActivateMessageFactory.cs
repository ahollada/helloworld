﻿using HelloWorld.DAO.Interface;

namespace HelloWorld.DAO
{
    public static class ActivateMessageFactory
    {
        public const string DatabaseActivateMessage = "database";
        public const string ConsoleActivateMessage = "console";

        public static IActivateMessage CreateActiveMessage(string type)
        {
            switch (type)
            {
                case DatabaseActivateMessage:
                    return new DatabaseMessage();
                case ConsoleActivateMessage:
                    return new ConsoleMessage();
                default:
                    return new ConsoleMessage();
            }
        }
    }
}