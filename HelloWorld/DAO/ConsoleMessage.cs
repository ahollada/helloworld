﻿using System;
using System.Diagnostics;
using HelloWorld.DAO.Interface;

namespace HelloWorld.DAO
{
    public class ConsoleMessage : IActivateMessage
    {
        public void ActivateMessage(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new Exception("Invalid Message");
            }
            Debug.WriteLine(message);
            Console.WriteLine(message);
        }
    }
}