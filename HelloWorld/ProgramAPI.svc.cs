﻿using System;
using System.Web.Configuration;
using HelloWorld.API.Interface;
using HelloWorld.API.Model;
using HelloWorld.BAO;

namespace HelloWorld
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ProgramAPI : IProgramAPI
    {
        private readonly MessageLogic messageLogic;
        public ProgramAPI()
        {
            string messageType = WebConfigurationManager.AppSettings["ActivateMessageType"];
            this.messageLogic = new MessageLogic(messageType);
        }
        public APIResponse DeliverMessage(string message)
        {
            try
            {
                messageLogic.DeliverMessageAccordingly(message);
            }
            catch (Exception e)
            {
                return new APIResponse {Succuss = false};
            }
            return new APIResponse {Succuss = true};
        }
    }
}