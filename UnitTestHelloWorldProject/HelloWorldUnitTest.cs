﻿using System;
using HelloWorld;
using HelloWorld.API.Interface;
using HelloWorld.API.Model;
using HelloWorld.BAO;
using HelloWorld.DAO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestHelloWorldProject
{
    [TestClass]
    public class HelloWorldUnitTest
    {
        [TestMethod]
        public void VerifyApiPrintMessage()
        {
            var myMessage = "Hello World!";
            APIResponse response = null;
            IProgramAPI myApi = new ProgramAPI();
            response = myApi.DeliverMessage(myMessage);

            Assert.AreEqual(true, response.Succuss);
        }

        [TestMethod]
        public void VerifyMessageLogic_Console_Load()
        {
            var myLogic = new MessageLogic(ActivateMessageFactory.ConsoleActivateMessage);
            var myLogicActivateMessage = myLogic.ActivateMessage;

            Assert.IsInstanceOfType(myLogicActivateMessage, typeof (ConsoleMessage));
        }

        [TestMethod]
        public void VerifyMessageLogic_Console()
        {
            try
            {
                var myLogic = new MessageLogic(ActivateMessageFactory.ConsoleActivateMessage);
                myLogic.DeliverMessageAccordingly("Hello World");
            }
            catch (Exception e)
            {
                Assert.Fail("no exception expected, but got this: " + e.Message);
            }
        }

        [TestMethod]
        [ExpectedException(typeof (NotImplementedException))]
        public void VerifyMessageLogic_Database()
        {
            var myLogic = new MessageLogic(ActivateMessageFactory.DatabaseActivateMessage);
            myLogic.DeliverMessageAccordingly("Hello World");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void VerifyMessageLogic_Console_No_Message()
        {
            var myLogic = new MessageLogic(ActivateMessageFactory.ConsoleActivateMessage);
            myLogic.DeliverMessageAccordingly("");
        }
    }
}